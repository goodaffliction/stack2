﻿#include <iostream>
#include <new>
using namespace std;

// класс, реализующий стек в виде динамического массива
template <typename T>
class Stack
{
private:
    T* stack; // Динамический масcив-указатель на стек
    int top, size; // Вершина стека - количество элементов типа T в стеке

public:
    // конструктор по умолчанию
    Stack()
    {
        size = 10;
        stack = new T[size];
        top = -1; // количество элементов в стеке
    }

    // помещаем элемент в стек
    void push(const T value)
    {
        if (top < size)
        {
            top++;
            stack[top] = value;
        }
        else
        {
            cout << "Stack is full";
        }
    }

    // Вытянуть элемент из стека
    T pop()
    {
        if (top == 0)
            return 0; // стек пуст
        top--;
        return stack[top];
    }

    // Просмотр элемента в вершине стека
    T Head()
    {
        if (top == 0)
            return 0;
        return stack[top - 1];
    }

        // Деструктор - освобождает память
    ~Stack()
    {
        if (top > 0)
            delete[] stack;
    }
    // Количество элементов в стеке
    int topStack()
    {
        return top;
    }
    // Функция, выводящая стек
    void Print()
    {
        T* p; // временный указатель, двигается по элементах стека

        // 1. Установить указатель p на вершину стека
        p = stack;

        // 2. Вывoд
        cout << "Stack: " << endl;
        if (top == 0)
            cout << "is empty." << endl;

        for (int i = 0; i <= top; i++)
        {
            cout << "Item[" << i << "] = " << *p << endl;
            p++; // прокрутить указатель на следующий элемент
        }
        cout << endl;
    }
};

int main()
{
    // объявить стек из целых чисел
    Stack <int> st;
    int   size = 0;
    int number;

    while (size++ < 9)
    {
        cin >> number;
        st.push(number);
    }
    cout << endl;

    st.Print();
    cout << "Count: " << st.topStack() << endl;

     // -1 item
    int t;
    t = st.pop(); // t = 7
    cout << "Delete item: " << t << endl;
    st.Print(); // 5, 9, 13
    cout << "Head: " << st.Head() << endl;

    // -2 items
    st.pop(); // st1 = { 5, 9 }
    st.pop(); // st1 = { 5 }
    st.Print();

    // -2 items
    st.pop(); // st1 = { }
    st.pop();
    st.Print();
}